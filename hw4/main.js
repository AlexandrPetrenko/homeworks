// создаем массив
let arr = [1, 2, 3, 4, 5,]; 
// создаем функцию которая принимает 2 значения
function map(arr, callback) {
  // создаем новый пустой массив куда в будущем запишем значения
  const result = [];
  // создаем переменную с длиной массива (не обязательно)
  const length = arr.length;
  // создаем цикл счетчик для каждого элемента массива
  for (let i = 0; i < length; i++) {
    // записываем каждый элемент массива в переменную
    const currentElement = arr[i];
    //Запускаем функцию колбек передавая в нее currentElement
    const callbackValue = callback(currentElement);
    //добавляем значение в пустой массив result[]
    result.push(callbackValue);
  }
  // возвращаем новый массив
  return result;
}
// вызываем функцию map передавая параметри 1) массив arr 2) стрелочная функция которая будет увеличивать значение каждого єлемента массива на 2
document.write(map(arr, (x) => x + 2));


// тоже самое тоько с методом map
// создаем основной массив и пустой массив в который будем записывать новые значения
let numbers = [1, 2, 3, 4, 5,];
let nambersNew = [];
//срздаем первую функцию которая будет принимать массив и фунцкию
function firstFunction (arr, fn) {
    // применяем к массиву метод map который переберает значения массива
    numbers.map(function (item) {
    // создаем 2 переменных для хранения значений и вызова коллбэк функции
    const thisElement = item;
    const fnValue = fn(thisElement);
    // добавляем значение в пустой массив
    nambersNew.push(fnValue);
  }) 
  // возвращаем пустой (уже не пустой) массив
  return nambersNew;
}
// вызываем первую функцию куда передаем массив и стрелочную функцию которая будет добавлять к старому значению +2
document.write(firstFunction(numbers, (x) => x + 2));


// задание 2
let age = prompt("Сколько лет");
function checkAge(age) {
  return (age > 18? true: confirm("Родители разрешили?"));
}
document.write(checkAge(age));