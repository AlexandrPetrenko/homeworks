// создаем переменные для промптов
let firstNumber = prompt("First number"), secondNumber = prompt("Second number"), math = prompt("Math action");
// создаем функции для 4-х математических действий
function add(firstNumber, secondNumber) {
    // если в промпте нажать ок или отмена то число будет = 0
    if (firstNumber === null || firstNumber === "") {
        firstNumber = 0;
    } else if (secondNumber === null || firstNumber === "") {
        secondNumber = 0
    }
    return +firstNumber + +secondNumber;
}
function min(firstNumber, secondNumber) {
    if (firstNumber === null || firstNumber === "") {
        firstNumber = 0;
    } else if (secondNumber === null || firstNumber === "") {
        secondNumber = 0
    }
    return +firstNumber - +secondNumber;
}
function del(firstNumber, secondNumber) {
    if (firstNumber === null || firstNumber === "") {
        firstNumber = 0;
    } else if (secondNumber === null || firstNumber === "") {
        secondNumber = 0
    }
    return +firstNumber / +secondNumber;
}
function multiple(firstNumber, secondNumber) {
    if (firstNumber === null || firstNumber === "") {
        firstNumber = 0;
    } else if (secondNumber === null || firstNumber === "") {
        secondNumber = 0
    }
    return +firstNumber * +secondNumber;
}
function calculate(firstNumber, secondNumber, fun) {
    // если число аргументов будет не равно 3 то вівести сообщение об ошибке
    if (arguments.length !== 3) {
        alert("error")
    } else {
        return fun(firstNumber, secondNumber);
    } 
}
// цикл который проверяет знак записанный в math и запускает соответствующий сценарий
if (math === "+") {
    document.write(calculate(firstNumber, secondNumber, add));
} else if (math === "-") {
    document.write(calculate(firstNumber, secondNumber, min));
} else if (math === "*") {
    document.write(calculate(firstNumber, secondNumber, multiple));
} else if (math === "/") {
    // делить на ноль нельзя
    if (+secondNumber === 0) {
        alert("error");
    } else {
        document.write(calculate(firstNumber, secondNumber, del));
    }
        
}


