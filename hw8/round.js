// создаем рандомайзер для цвета круга
// в переменной а будет хранится цвет
let a;
function randomColor () {
    // создаем генератор случайного числа
    const color = Math.round(Math.random() * 10)
    // и передаем значение цвета в переменную
    if (color === 0) {
        a = "red";
    } else if (color === 1) {
        a = "blue"
    } else if (color > 1 && color < 5) {
        a = "green";
    } else if (color > 5 && color < 8) {
        a = "yellow"; 
    } else {
        a = "orange";
    }
}
randomColor();
// получаем созданую в html кнопку по id
const button = document.querySelector("#button");
// при нажатии на кнопку создаем инпут и вторую кнопку
button.onclick = function () {
    const input = document.createElement("input");
    const draw = document.createElement("button");
    input.type = "text";
    draw.textContent = "Нарисовать";
    document.body.appendChild(input);
    document.body.appendChild(draw);
    // при нажатии на вторую кнопку создаем контейнер для кружков и сами кружки
    draw.onclick = function () {
        // в переменную передаем значение инпута и переводим его в число
       const dia = +input.value;
    //    создаем контейнер и стили для него
       const container = document.createElement("div");
       container.style.margin = "0 auto";
    //    ширина и высота будет менятся чтобы по ширине влазило только 10 кружков
       container.style.width = `${dia * 10}px`;
       container.style.height = `${dia * 18}px`;
       document.body.appendChild(container);
    //    функция конструктор кружков
       function NewCircle () {
            const circle = document.createElement("span");
            circle.style.background = `${a}`;
            // диаметр зависит от введенного в инпут значения
            circle.style.width = `${dia}px`;
            circle.style.height = `${dia}px`;
            circle.style.display = "inline-block";
            circle.style.borderRadius = "50%";
            container.appendChild(circle);
       }
    //    вызываем ее 100 раз
       for (i = 0; i < 100; i++) {
           let a = new NewCircle();
       }
    //    из всех кружков создаем массив
       const [...circleArr] = document.querySelectorAll("span");
    //    циклом вешаем кружкам удаление по нажатию
       circleArr.forEach(element => {
        element.onclick =  function() {
            this.remove();
        }
       });
    }
    
}
