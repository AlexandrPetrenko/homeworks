//Нашел метод dispatch в интернете, создал для него event. применяю на 102 строчке
const event = new Event("click");
// создю кнопку для новой игры
const newGame = document.createElement("button");
newGame.textContent = "Начать новую игру";
document.body.appendChild(newGame);
// при клике на кнопку произвести перезагрузку страницы
newGame.onclick = function () {
    location.reload();
}
blockButtons = false;
// создаю контейнер для игры
const section = document.createElement("section");
// стили для контейнера
section.style.background = "grey";
section.style.width = "280px";
section.style.height = "280 px";
section.style.margin = "0 auto";
document.body.appendChild(section);
// 64 клетки для игры
for (i = 0; i < 64; i++) {
    const item = document.createElement("div");
    item.style.background = "white";
    item.style.width = "35px";
    item.style.height = "35px";
    item.style.border = "1px solid black";
    item.style.display = "inline-block";
    item.style.verticalAlign = "top";
    item.style.textAlign = "center";
    item.style.boxSizing = "border-box";
    section.appendChild(item);
}
// создаю массив из клеток
const [...arr] = document.querySelectorAll("div");
// создаю 10 случайно расположенных мин
for (j = 0; j < 10; j++) {
    // мины и их стили
    const mine = document.createElement("span");
    mine.style.display = "inline-block";
    mine.style.width =  "100%";
    mine.style.height = "100%";
    // я скрыл мины через прозрачность, можно и через visibility и через размер в 0px и через display: none
    mine.style.background = "transparent";
    // функция которая создает случайное число от и до
    function getRandomIntInclusive(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    // создать мину в поле если ее там еще нет
    const minePosition = getRandomIntInclusive(0, 63);
    if (arr[minePosition].firstChild === null) {
        arr[minePosition].appendChild(mine);
    }
    // меняем цвет мине при нажатии на нее
    mine.onclick = function () {
        blockButtons = true;
        const [...mineArr] = document.querySelectorAll("span");
        mineArr.forEach(element => {
            element.style.background = "red";
        });
    }
}
// каждому элементу массиву из div добавляем событие при нажатии с помощью for each который будет передавать еще и индекс
arr.forEach(function (element, index)  {
    // при нажатии на div вызывать функцию
    element.onclick = function () {
        // создаем пустой массив (можно просто с переменной, но с массивом интереснее)
        const mineNumber = [];
        if (blockButtons) {
            return
        } else {
             // проверяем, есть ли в диве мина
        if (element.firstChild === null) {
            // если мины нету, то нужно проверить на какой div мы нажали, ибо логика для ряда вначале, середине и конце отличается.
            // первый ряд это индексы 0 8 16 24 32 40 48 56
            if (index === 0 || index === 8 || index === 16 || index === 24 || index === 32 || index === 40 || index === 48 || index === 56) {
                // если нажали на один из них, то нужно проверять div правее (div + 1), снизу и сверху +8 и -8 и диагонали справа от єлемента +9 - 7, если такие существуют
                if (arr[index + 1] !== undefined) {
                    // если такой div существует, то мы проверяем его дочерний элемент, если он есть то мы записываем его в массив, также дальше в dive будет появляться цифра которая означает количество мин вокруг, чтобы она не воспринималась как мина мы добавляем что дочерний элемент с текстовым нод тайпом не считается
                    if (arr[index + 1].firstChild !== null && arr[index + 1].firstChild.nodeType !== 3) {
                          mineNumber.push(arr[index + 1].firstChild);
                      }
                  }
                    if (arr[index + 8] !== undefined) {
                        if (arr[index + 8].firstChild !== null && arr[index + 8].firstChild.nodeType !== 3) {
                            mineNumber.push(arr[index + 8 ].firstChild);
                        }
                    }  
                    if (arr[index - 8] !== undefined) {
                        if (arr[index - 8].firstChild !== null && arr[index - 8].firstChild.nodeType !== 3) {
                            mineNumber.push(arr[index - 8 ].firstChild);
                        }
                    }
                    if (arr[index + 9] !== undefined) {
                        if (arr[index + 9].firstChild !== null && arr[index + 9].firstChild.nodeType !== 3) {
                            mineNumber.push(arr[index + 9].firstChild);
                        }
                    }
                    if (arr[index - 7] !== undefined) {
                        if (arr[index - 7].firstChild !== null && arr[index - 7].firstChild.nodeType !== 3) {
                            mineNumber.push(arr[index - 7].firstChild);
                        }
                    }
                    // после проверки всех соседних клеток мы записываем в нажатый div длину массива, таким образом клетка показывает сколько вокруг нее мин
                    element.textContent = mineNumber.length;
                    // если мин вокруг нету
                    if (mineNumber.length === 0) {
                        // перекрашиваем div в зеленый
                        element.style.background = "green";
                        // смотреть строку 2. там мы создали ивент click который имитирует нажатие на элемент и вызываем его в нужный для нас момент с помощью метода dispatchEvent
                        //если в нажатой кнопке нету мин то мы вызываем click на соседних чем вызываем onclick на них
                        //сейчас мы описываем первую колонку, у всех ее элементов есть элемент справа
                        arr[index + 1].dispatchEvent(event);
                        // а вот под элементом, над ним и по диагонали может и не быть, по этому делаем проверку на существование элемента
                        if (arr[index + 8] !== undefined) {
                            arr[index + 8].dispatchEvent(event);
                        }
                        if (arr[index - 8] !== undefined) {
                            arr[index - 8].dispatchEvent(event);
                        }
                        if (arr[index + 9] !== undefined) {
                            arr[index + 9].dispatchEvent(event);
                        }
                        if (arr[index - 7] !== undefined) {
                            arr[index - 7].dispatchEvent(event);
                        }
                    }
                    // тоже самое для последней колонки
            } else if (index === 7 || index === 15 || index === 23 || index === 31 || index === 39 || index === 47 || index === 55 || index === 63) {
                if (arr[index - 1] !== undefined) {
                              if (arr[index - 1].firstChild !== null && arr[index - 1].firstChild.nodeType !== 3) {
                                  mineNumber.push(arr[index - 1].firstChild);
                              }
                          }
                          if (arr[index + 8] !== undefined) {
                              if (arr[index + 8].firstChild !== null && arr[index + 8].firstChild.nodeType !== 3) {
                                  mineNumber.push(arr[index + 8 ].firstChild);
                              }
                          }  
                          if (arr[index - 8] !== undefined) {
                            if (arr[index - 8].firstChild !== null && arr[index - 8].firstChild.nodeType !== 3) {
                                mineNumber.push(arr[index - 8 ].firstChild);
                            }
                        }  
                          if (arr[index + 7] !== undefined) {
                              if (arr[index + 7].firstChild !== null && arr[index + 7].firstChild.nodeType !== 3) {
                                  mineNumber.push(arr[index + 7 ].firstChild);
                              }
                          }
                        if (arr[index - 9] !== undefined) {
                            if (arr[index - 9].firstChild !== null && arr[index - 9].firstChild.nodeType !== 3) {
                                mineNumber.push(arr[index - 9].firstChild);
                            }
                        }
                          element.innerHTML = mineNumber.length;
                          if (mineNumber.length === 0) {
                              element.style.background = "green";
                              arr[index - 1].dispatchEvent(event);
                              if (arr[index + 8] !== undefined) {
                                arr[index + 8].dispatchEvent(event);
                            }
                            if (arr[index - 8] !== undefined) {
                                arr[index - 8].dispatchEvent(event);
                            }
                            if (arr[index - 9] !== undefined) {
                                arr[index - 9].dispatchEvent(event);
                            }
                            if (arr[index + 7] !== undefined) {
                                arr[index + 7].dispatchEvent(event);
                            }
                          }
                        //   и для середины
            } else {
                if (arr[index + 1] !== undefined) {
                    if (arr[index + 1].firstChild !== null && arr[index + 1].firstChild.nodeType !== 3) {
                          mineNumber.push(arr[index + 1].firstChild);
                      }
                  }
                    if (arr[index - 1] !== undefined) {
                        if (arr[index - 1].firstChild !== null && arr[index - 1].firstChild.nodeType !== 3) {
                            mineNumber.push(arr[index - 1].firstChild);
                        }
                    }
                    if (arr[index + 8] !== undefined) {
                        if (arr[index + 8].firstChild !== null && arr[index + 8].firstChild.nodeType !== 3) {
                            mineNumber.push(arr[index + 8 ].firstChild);
                        }
                    }  
                    if (arr[index - 8] !== undefined) {
                        if (arr[index - 8].firstChild !== null && arr[index - 8].firstChild.nodeType !== 3) {
                            mineNumber.push(arr[index - 8 ].firstChild);
                        }
                    }
                    if (arr[index - 9] !== undefined) {
                        if (arr[index - 9].firstChild !== null && arr[index - 9].firstChild.nodeType !== 3) {
                            mineNumber.push(arr[index - 9].firstChild);
                        }
                    } 
                    if (arr[index + 9] !== undefined) {
                        if (arr[index + 9].firstChild !== null && arr[index + 9].firstChild.nodeType !== 3) {
                            mineNumber.push(arr[index + 9].firstChild);
                        }
                    }
                    if (arr[index - 7] !== undefined) {
                        if (arr[index - 7].firstChild !== null && arr[index - 7].firstChild.nodeType !== 3) {
                            mineNumber.push(arr[index - 7].firstChild);
                        }
                    }
                    if (arr[index + 7] !== undefined) {
                        if (arr[index + 7].firstChild !== null && arr[index + 7].firstChild.nodeType !== 3) {
                            mineNumber.push(arr[index + 7].firstChild);
                        }
                    }
                    element.textContent = mineNumber.length;
                    if (mineNumber.length === 0) {
                        element.style.background = "green";
                        arr[index + 1].dispatchEvent(event);
                        arr[index - 1].dispatchEvent(event);
                        if (arr[index + 8] !== undefined) {
                            arr[index + 8].dispatchEvent(event);
                        }
                        if (arr[index - 8] !== undefined) {
                            arr[index - 8].dispatchEvent(event);
                        }
                        if (arr[index + 9] !== undefined) {
                            arr[index + 9].dispatchEvent(event);
                        }
                        if (arr[index - 9] !== undefined) {
                            arr[index - 9].dispatchEvent(event);
                        }
                        if (arr[index - 7] !== undefined) {
                            arr[index - 7].dispatchEvent(event);
                        }
                        if (arr[index + 7] !== undefined) {
                            arr[index + 7].dispatchEvent(event);
                        }
                    }
            }
        }
        }
       
    }

});

// создаем действие на клике правой кнопкой мыши
// создаем пустой массив (можно и с переменной но с массивом интереснее)
const flags = [];
// создаем счетчик флажков
const counterItem = document.createElement("p");
section.before(counterItem);
// создаем функцию которая будет изменять количество флажков в счетчике
function counter () {
    counterItem.textContent = `Найдено ${flags.length} / 10`;
}
// всем дивам в массиве из 25-й строчки присваиваем действие при нажатии правой кнопкой
arr.forEach(element => {
    element.oncontextmenu = function(event) {
        event.preventDefault();
        if (blockButtons) {
            return
        } else {
            // если у элемента textContent это пустая строка то мы создаем span и до который будет отображать флаг и добавляем этот элемент как первый дочерний и добавляем в массив
        if (element.textContent === "") {
            const flag = document.createElement("span");
            flag.innerHTML = "&#9971;";
            element.prepend(flag);
            flags.push(flag);
        }
        // а если уже не пустой то удаляем элемент
        else {
            element.firstChild.remove();
            flags.pop();
        }
        // и вызываем функцию из 239 строки которая изменит цифру в счетчике на длину массива флажков
        counter();
        }   
    }
        
});
// P.S. тут есть нюанс, чтобы открывались все соседние клетки то надо вызывать dispatchEvent для новых и новых соседних кнопок, принцип я понял, а делать код на 400+ строк не хотелось)))