// первое задание которое было сначало
// Создаем переменную button которая будет добавлять button в DOM
const button = document.createElement("button");
// Добавляем в нее текст
button.textContent = "Создать новый див";
// добавляем кнопку в body
document.body.append(button);
// создаем функцию которая будет создавать div
function createDiv() {
    // создаем переменную которая будет добавлять div в DOM
    const div = document.createElement("div");
    // добавляем текст в div
    div.textContent = "Новый div";
    // добавляем div после button
    button.after(div);
    // вызываем функцию по удалению div
    delElements();
}
// создаем функцию по удалению div
function delElements() {
    // создаем коллекццию из div
    const divCount = document.querySelectorAll("div");
    // если длина коллекции равна > 10 то все div удаляются
    if (divCount.length > 10) {
        divCount.forEach(element => {
            element.remove();
        });
    }
}
// вызываем функцию при клике на button
button.onclick = createDiv;