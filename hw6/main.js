// Задание 1 классная работа
// создаем функцию конструктор
function Calculator() {
    // создаем метод сбора данных который запишет их как аргумент и значение обьекта
    this.read = function() {
        // пользователь введет значения для аргументов
        this.firstNum = prompt("Enter first number");
        this.secondNum = prompt("Enter second number");
        // вызов методов сложения и умножения значений аргументов обьета
        this.sum();
        this.mul();
    }
    // метод сложения аргументов обьекта и вовод результата в консоль
    this.sum = function() {
        console.log(+this.firstNum + +this.secondNum);
    }
    // метод умножения аргументов обьекта и вовод результата в консоль
    this.mul = function() {
        console.log(+this.firstNum * +this.secondNum);  
    }
    // вызов метода заполнения аргументов обьекта
    this.read();
}
// вызов функции конструктора
const calc = new Calculator();

// Задание 1 классна работа вариант с прототипами 
function CalculatorPrototype () {
    this.firstNumProto = prompt("Enter first number");
    this.secondNumProto = prompt("Enter second number");
    this.sumProto();
    this.mulProto();
}
//методы умножения и сложения вынесены в прототип
CalculatorPrototype.prototype.sumProto = function() {
    console.log(+this.firstNumProto + +this.secondNumProto);
}
CalculatorPrototype.prototype.mulProto = function() {
    console.log(+this.firstNumProto * +this.secondNumProto);
}
const calcProto = new CalculatorPrototype();

// задание 1 домашняя работа (для уменьшения в пузырьковом методе надо просто поменять знак > на <)
//создаем функцию конструктор которая принимает 2 аргумента
function Human(firstName, age) {
    this.firstName = firstName;
    this.age = age;
} 
// вызываем функцию конструктор 5 раз, тем самым создаем 5 обьектов передаем в значение имя и возраст 
const newUserAl = new Human("Alexandr", 28);
const newUserVl = new Human("Vladimir", 22);
const newUserOl = new Human("Olga", 35);
const newUserSv = new Human("Svetlana", 19);
const newUserJu = new Human("Julia", 23);
// создаем массив обьектов
const allUsers = [newUserAl, newUserVl, newUserOl, newUserSv, newUserJu,];
// пузырьковый метод (украл в интернете, там был и по ES6 но он показался мне сложным)
function bubbleSort(allUsers) {
    // задаем количество итераций для обхода всего массива
    for (i = 0; i < allUsers.length; i++) {
        //задаем количество итераций для сравнения соседних чисел
        for (j = 0; j < allUsers.length - 1; j++) {
            // если значение свойства age у элемента больше чем у следующего
            if (allUsers[j].age > allUsers[j + 1].age) {
                // то записываем его в переменную
                const swap = allUsers[j].age;
                // и меняем их местами путем изменения индекса
                allUsers[j].age =  allUsers[j + 1].age;
                allUsers[j + 1].age = swap;
            }
        }
    }
    // перебераем все элементы массива и выводим их значения
    allUsers.forEach(element => {
        document.write(`${element.firstName} : ${element.age} </br>`)
    });
}
bubbleSort(allUsers);

// Задание 2 перпеделал
// создаем функцию конструктор
function NewWorker(position, surname, salary, workMonth) {
        this.position = position;
        this.surname = surname;
        this.salary = salary;
        this.workMonth = workMonth;
    }
// промпты для заполнения данных 
const position = prompt("Укажите должность");
const surname = prompt("Укажите фамилию");
const salary = prompt("Укажите оклад");
const workMonth = prompt("Укажите стаж сотрудника");
// создаем обьект передаем туда данные из промпта
const worker = new NewWorker(position, surname, salary, workMonth, surname, salary, workMonth);
// метод конструктора для подсчета
NewWorker.salary = function() {
    document.write(`Сотрудник ${worker.surname} заработал ${+worker.salary * +worker.workMonth}`);
}
// прототип конструктора для вывода данных нового обьекта на экран
NewWorker.prototype.screenCheck = function() {
    document.write(`</br> ${this.position} </br> ${this.surname} </br> ${this.salary} </br> ${this.workMonth} </br>`);
}
// вызов методов
NewWorker.salary();
worker.screenCheck();