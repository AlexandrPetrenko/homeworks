//создаем обьект и вложенный обьект с пустыми значениями свойств
const Document = {
    heading : "",
    body : "", 
    footer : "", 
    data : "",
    add : {
        heading : "",
        body : "", 
        footer : "", 
        data : "",
    },
    //создаем функцию для заполнения и отображения приложения
    addAdd() {
        this.add.heading = prompt("Введите заголовок");
        this.add.body = prompt("Заполните тело");
        this.add.footer = prompt("Заполните подвал");
        this.add.data = prompt("Заполните дату");
        document.write(`Заголовок приложения: ${this.add.heading} ${"<br/>"}`);
        document.write(`Тело приложения: ${this.add.body} ${"<br/>"}`);
        document.write(`Подвал приложения: ${this.add.footer} ${"<br/>"}`);
        document.write(`Дата приложения: ${this.add.data} ${"<br/>"}`);
    },
    //создаем функцию для заполнения и отображения документа
    contantAdd() {
        this.heading = prompt("Введите заголовок");
        this.body = prompt("Заполните тело");
        this.footer = prompt("Заполните подвал");
        this.data = prompt("Заполните дату");
        document.write(`Заголовок: ${this.heading} ${"<br/>"}`);
        document.write(`Тело: ${this.body} ${"<br/>"}`);
        document.write(`Подвал: ${this.footer} ${"<br/>"}`);
        document.write(`Дата: ${this.data} ${"<br/>"}`);
        //если пользователь хочет заполнить приложения то вызываем первую функцию
        let conf = confirm("Заполнить приложение?");
        if (conf === true) {
            this.addAdd();
        }
    },
}
//вызываем основную функцию
Document.contantAdd();

