// задание 2 и 3
//Тело для функции-конструктора
function CreateNewUser(fn, ln, bd) {
    this.firstName = fn;
    this.lastName = ln;
    this.birthDay = bd;
}
//функции для вывода в консоль записываем в прототип
CreateNewUser.prototype.getLogIn = function() {
    console.log(`${this.firstName[0].toLowerCase()}${this.lastName.toLowerCase()}`);
}
CreateNewUser.prototype.getAge = function() {
    console.log(2020 - +this.birthDay.substring(6));
}
CreateNewUser.prototype.getPassword = function() {
    console.log(`${this.firstName[0].toUpperCase()}${this.lastName.toLowerCase()}${this.birthDay.substring(6)}`);
}
//создаем функцию которая будет соберать данніе от пользователя и передовать их в функцию конструктор
function data() {
    let fn = prompt("first name");
    let ln = prompt("last name");
    let bd = prompt("birthday", "dd.mm.yyyy");
    let newUser = new CreateNewUser(fn, ln, bd);
    newUser.getLogIn();
    newUser.getAge();
    newUser.getPassword();
}
//запуск функции для сбора данных и запуска функции-конструктора
data();

// задание 4
// создаем массив с разными типами данных
const arr = [1, 2, "string", true, "string2", "15", 15, false,];
// создаем функцию которая принимает массив и тип данных и будет с ними работать
function filterBy(arr, dataType) {
    // создаем новый массив с помощью фильтра старого массива по типу данных, с помощью метода фильтр. тип элемента не равен заданному то он проходит в новый массив
    const newArr = arr.filter(element => typeof element !== dataType);
    // выводим в консоль новый массив
    console.log(newArr);
}
// создаем функцию для сбора типа данных и запуска функции фильтра с передачей туда нужных значений
function userDataType () {
    let dataTypeValue = prompt("Какой тип даннных надо удалить?").toLowerCase();
    filterBy(arr, dataTypeValue);
}
userDataType();
